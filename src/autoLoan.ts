// import * as fs from "fs";

// let inputString: string[];
// let str = (fs.readFileSync("./input.txt")).toString();
// let currentLine = 0;
// inputString = str.trim().split('\n').map((string: any) => {
// 	return string.trim();
// });

process.stdin.resume();
process.stdin.setEncoding('utf-8');

let inputString: any = '';
let currentLine = 0;

process.stdin.on('data', (inputStdin: any) => {
	inputString += inputStdin;
});

process.stdin.on('end', (listener: any) => {
	inputString = inputString.trim().split('\n').map((string: any) => {
		return string.trim();
	});

	main();
});

function readLine() {
	return inputString[currentLine++];
}

function main() {
	const line = readLine().split(" ").map((x: any) => +x);
	console.log(calculate(line[0], line[1], line[2]));
}

function calculate(price: number, monthlyPayment: number, loanTerm: number): number {

	const N = 100;
	let left = 0;
	let right = 100;

	for (let i = 0; i < N; i++) {
		let tmp = left + (right - left) / 2;
		let currentPrice = price;
		for (let j = 0; j < loanTerm; j++) {
			currentPrice = currentPrice + currentPrice * tmp / 100;
			currentPrice -= monthlyPayment;
			if (price < currentPrice)
				break;
		}

		if (currentPrice <= 0)
			left = tmp;
		else
			right = tmp;
	}

	return left * 12;
}