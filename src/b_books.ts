// https://codeforces.com/problemset/problem/279/B?locale=en

import readline from "readline";

const rl = readline.createInterface({
	input: process.stdin,
	output: process.stdout,
});
let input: any = [];

rl.on("line", (line: any) => {
	input.push(line);
	if (input.length === 2) {
		const a = input[0].split(" ").map((x: any) => +x);
		const b = input[1].split(" ").map((x: any) => +x);
		console.log(calculate(a, b));
		input = []
	}
});
rl.on("close", () => { });

export function calculate(data: number[], bookTimes: number[]): number {

	const bookCount = data[0];
	const freeTime = data[1];

	let counter = 0;
	let sum = 0;
	let j = 0;

	for (let i = 0; i < bookCount; i++) {
		sum += bookTimes[i];
		if (sum <= freeTime)
			counter++;
		else {
			sum = sum - bookTimes[j];
			j++;
		}
	}
	return counter;
}