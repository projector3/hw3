// https://cses.fi/problemset/task/1660

export function getSubarray(items: number[], array: number[]): number {

	const length = items[0];
	const xSum = items[1];
	let left = 0
	let right = 0;
	let counter = 0;
	let sum = array[0];
	while (left < length) {
		if (sum === xSum) {
			counter++;
			sum -= array[left];
			left++;
			right++;
			sum += array[right];
		} else if (sum < xSum) {
			right++;
			sum += array[right];
		} else {
			sum -= array[left];
			left++;
		}
	}
	return counter;
}