// // https://codeforces.com/problemset/problem/190/D?locale=en

// process.stdin.resume();
// process.stdin.setEncoding('utf-8');

// let inputString: any = '';
// let currentLine = 0;

// process.stdin.on('data', inputStdin => {
// 	inputString += inputStdin;
// });

// process.stdin.on('end', _ => {
// 	inputString = inputString.trim().split('\n').map((string: any) => {
// 		return string.trim();
// 	});

// 	main();
// });

// function readLine() {
// 	return inputString[currentLine++];
// }

// function main() {
// 	const [count, k] = readLine().split(' ').map((x: any) => +x);
// 	const arr = readLine().split(' ').map((x: any) => +x);
// 	if (count === arr.length) {
// 		console.log(calculate(arr, k));
// 		currentLine = 0;
// 		inputString = '';
// 	}
// }

// function calculate(array: number[], k: number): number {

// 	let result = 0;
// 	const map: { [num: number]: number } = {};

// 	let idx = 0;
// 	for (let i = 0; i < array.length; i++) {
// 		const item = array[i];
// 		map[item] = (map[item] || 0) + 1;
// 		while (map[item] >= k && idx <= i) {
// 			result += array.length - i;
// 			const item = array[idx];
// 			map[item] = (map[item] || 0) - 1;
// 			idx++;
// 		}

// 	}

// 	return result;
// }