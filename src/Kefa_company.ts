// // https://codeforces.com/problemset/problem/580/B?locale=en

// process.stdin.resume();
// process.stdin.setEncoding('utf-8');

// let inputString: any = '';
// let currentLine = 0;

// process.stdin.on('data', inputStdin => {
// 	inputString += inputStdin;
// });

// process.stdin.on('end', _ => {
// 	inputString = inputString.trim().split('\n').map((string: any) => {
// 		return string.trim();
// 	});

// 	main();
// });

// function readLine() {
// 	return inputString[currentLine++];
// }

// function main() {
// 	const [count, minimalMoney] = readLine().split(' ').map((x: any) => +x);
// 	const array = [];
// 	while (currentLine <= count) {
// 		array.push(readLine().split(' ').map((x: any) => +x));
// 	}
// 	if (count === array.length) {
// 		console.log(calculate(array, minimalMoney));
// 		currentLine = 0;
// 		inputString = '';
// 	}
// }

// function calculate(array: number[][], minimalMoney: number) {

// 	array.sort((a, b) => a[0] - b[0]);

// 	let maxFriendshipCoeff = 0;
// 	let currentFriendshipCoeff = 0;
// 	let left = 0;

// 	for (let i = 0; i < array.length; i++) {
// 		while (array[i][0] - array[left][0] >= minimalMoney) {
// 			currentFriendshipCoeff -= array[left][1];
// 			left++;
// 		}

// 		currentFriendshipCoeff += array[i][1];
// 		maxFriendshipCoeff = Math.max(maxFriendshipCoeff, currentFriendshipCoeff);
// 	}

// 	return maxFriendshipCoeff;
// }