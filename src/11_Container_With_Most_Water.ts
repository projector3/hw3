// https://leetcode.com/problems/container-with-most-water/

export function maxArea(height: number[]): number {

	let maxArea = 0;
	let left = 0;
	let right = height.length - 1;
	while (left < right) {
		const area = calculateArea({ h: height[left], w: left }, { h: height[right], w: right });
		if (area > maxArea)
			maxArea = area;
		if (height[left] < height[right])
			left++;
		else if (height[left] > height[right])
			right--;
		else {
			left++;
			right--;
		}
	}
	return maxArea;
}

function calculateArea(coord1: { h: number; w: number }, coord2: { h: number; w: number }) {
	const height = Math.min(coord1.h, coord2.h);
	const width = coord2.w - coord1.w;
	return height * width;
}